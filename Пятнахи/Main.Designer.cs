﻿namespace Пятнахи
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.NewGameMM = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitMM = new System.Windows.Forms.ToolStripMenuItem();
            this.RatingMM = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewGameMM,
            this.ExitMM,
            this.RatingMM});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(232, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "Main Menu";
            // 
            // NewGameMM
            // 
            this.NewGameMM.Name = "NewGameMM";
            this.NewGameMM.Size = new System.Drawing.Size(81, 20);
            this.NewGameMM.Text = "Новая игра";
            this.NewGameMM.Click += new System.EventHandler(this.NewGameMM_Click);
            // 
            // ExitMM
            // 
            this.ExitMM.Name = "ExitMM";
            this.ExitMM.Size = new System.Drawing.Size(53, 20);
            this.ExitMM.Text = "Выход";
            this.ExitMM.Click += new System.EventHandler(this.ExitMM_Click);
            // 
            // RatingMM
            // 
            this.RatingMM.Name = "RatingMM";
            this.RatingMM.Size = new System.Drawing.Size(63, 20);
            this.RatingMM.Text = "Рейтинг";
            this.RatingMM.Click += new System.EventHandler(this.RatingMM_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 261);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пятнахи";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem NewGameMM;
        private System.Windows.Forms.ToolStripMenuItem ExitMM;
        private System.Windows.Forms.ToolStripMenuItem RatingMM;


    }
}

