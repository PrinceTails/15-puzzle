﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Пятнахи
{
    public partial class Main : Form
    {
        Button[] buttons;
        DateTime time;
        int Ticks;

        public Main()
        {
            InitializeComponent();
            CreateGame();
            if(Properties.Settings.Default.flagSave)
            {
                if (MessageBox.Show("Загрузить последнюю игру?", "Найдена не законченная игра", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    LoadGame();
                else
                {
                    Properties.Settings.Default.flagSave = false;
                    Properties.Settings.Default.Save = "";
                    Properties.Settings.Default.Save();

                    Shuffle();
                }
            }
            else
                Shuffle();
            
        }

        void CreateGame()
        {
            buttons = new Button[16];
            for (int i = 0; i < 16; i++)
            {
                buttons[i] = new Button();
                buttons[i].Text = (i + 1).ToString();
                buttons[i].Size = new Size(50, 50);
                buttons[i].Click += button_Click;
                buttons[i].TabIndex = i + 1;
            }

            buttons[15].Visible = false;
            buttons[15].Text = "";
            int n = 0;
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    buttons[n].Location = new Point(y * 50 + 15, x * 50 + 40);
                    this.Controls.Add(buttons[n]);
                    n++;
                }
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button a = (Button)sender;
            int current_Button = a.TabIndex-1;

            if(current_Button - 1 >= 0)
            {
                if ((current_Button + 1) % 4 != 1)
                {
                    checkButton(buttons[current_Button], buttons[current_Button - 1]);
                }
            }
            if (current_Button + 1 < 16)
            {
                if ((current_Button + 1) % 4 != 0)
                {
                    checkButton(buttons[current_Button], buttons[current_Button + 1]);
                }
            }
            if (current_Button - 4 >= 0)
            {
                checkButton(buttons[current_Button], buttons[current_Button - 4]);
            }
            if (current_Button + 4 < 16)
            {
                checkButton(buttons[current_Button], buttons[current_Button + 4]);
            }

            CheckWin();

        }

        void checkButton(Button a, Button b)
        {
            if(!b.Visible)
            {
                b.Text = a.Text;
                a.Text = "";
                a.Visible = false;
                b.Visible = true;
                Ticks++;
            }
        }

        void CheckWin()
        {
            for(int i=0;i<buttons.GetLength(0)-1;i++)
            {
                if (buttons[i].Text != (i + 1).ToString())
                    return;
            }
            int res = (DateTime.Now - time).Seconds;
            Rating rt = new Rating(Ticks, res);
            rt.ShowDialog();
            if (MessageBox.Show("Начать новую игру?", "Вы победили!", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Shuffle();
            else
                this.Close();
        }

        void Shuffle()
        {
            List<int> list = new List<int>();
            for (int i = 1; i < 16; i++)
                list.Add(i);
            Random r = new Random();
            for (int i = 0; i < 15; i++)
            {
                int t = r.Next(0, list.Count);
                buttons[i].Text = list[t].ToString();
                list.Remove(list[t]);

            }
            time = DateTime.Now;
            CheckWin();
            Ticks = 0;
        }

        private void NewGameMM_Click(object sender, EventArgs e)
        {
            Shuffle();
        }

        private void ExitMM_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void SaveGame()
        {
            string save = "";
            for(int i=0;i<buttons.GetLength(0);i++)
            {
                if (buttons[i].Visible == false)
                    save += "NO ";
                else
                    save += buttons[i].Text+" ";
            }

            Properties.Settings.Default.Save = save;
            Properties.Settings.Default.flagSave = true;
            Properties.Settings.Default.Save();
        }
        void LoadGame()
        {
            string save = Properties.Settings.Default.Save;
            var s = save.Split(' ');
            for(int i=0;i<buttons.GetLength(0);i++)
            {
                if(s[i]=="NO")
                {
                    buttons[i].Text = "";
                    buttons[i].Visible = false;
                }
                else
                {
                    buttons[i].Text = s[i];
                    buttons[i].Visible = true;
                }
            }

            Properties.Settings.Default.Save = "";
            Properties.Settings.Default.flagSave = false;
            Properties.Settings.Default.Save();

        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MessageBox.Show("Сохранить игру?", "Игра не окончена", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                SaveGame();
        }

        private void RatingMM_Click(object sender, EventArgs e)
        {
            Rating rt = new Rating();
            rt.ShowDialog();
        }
    }
}
