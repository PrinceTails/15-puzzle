﻿namespace Пятнахи
{
    partial class Rating
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stepsBox = new System.Windows.Forms.GroupBox();
            this.timeBox = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // stepsBox
            // 
            this.stepsBox.Location = new System.Drawing.Point(12, 12);
            this.stepsBox.Name = "stepsBox";
            this.stepsBox.Size = new System.Drawing.Size(117, 237);
            this.stepsBox.TabIndex = 0;
            this.stepsBox.TabStop = false;
            this.stepsBox.Text = "Ходы";
            // 
            // timeBox
            // 
            this.timeBox.Location = new System.Drawing.Point(155, 12);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(117, 237);
            this.timeBox.TabIndex = 1;
            this.timeBox.TabStop = false;
            this.timeBox.Text = "Время";
            // 
            // Rating
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.stepsBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Rating";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Рейтинг";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Rating_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox stepsBox;
        private System.Windows.Forms.GroupBox timeBox;
    }
}