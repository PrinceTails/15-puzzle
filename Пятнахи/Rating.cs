﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Пятнахи
{
    public partial class Rating : Form
    {
        Label[] steps;
        Label[] time;
        bool changed;
        public Rating()
        {
            InitializeComponent();
            changed = false;
            CreateRating();
        }
        public Rating (int Ticks, int Time)
        {
            InitializeComponent();
            changed = false;
            CreateRating();
            InsertRating(Ticks,Time);
        }

        void CreateRating()
        {
            CheckFile();
            StreamReader file = new StreamReader(Application.StartupPath + "\\rating.rat");
            string ff = file.ReadToEnd();
            file.Close();

            var res = ff.Split(' ');
            steps = new Label[10];
            for(int i=0;i<10;i++)
            {
                steps[i] = new Label();
                steps[i].Text = (i + 1).ToString()+". " + res[i];
                steps[i].Location = new Point(6, 20 + 21 *i);
                stepsBox.Controls.Add(steps[i]);
            }

            time = new Label[10];
            int t = 10;
            for(int i=0;i<10;i++)
            {
                time[i] = new Label();
                time[i].Text = (i + 1).ToString() + ". " + res[t];
                time[i].Location = new Point(6, 20 + 21 * i);
                timeBox.Controls.Add(time[i]);
                t++;
            }
        }

        void InsertRating(int ticks, int tm)
        {
            #region Вставить в рейтинг лучший результат по ходам
            for (int i=0;i<10;i++)
            {
                
                var s = steps[i].Text.Split(' ');
                if (s[1] == "-")
                {
                    steps[i].Text = (i + 1).ToString() + ". " + ticks;
                    steps[i].ForeColor = Color.Red;
                    changed = true;
                    break;
                }
                else
                {
                    int c = Convert.ToInt32(s[1]);
                    if (ticks < c)
                    {
                        MoveSteps(i);
                        steps[i].Text = (i + 1).ToString() + ". " + ticks;
                        steps[i].ForeColor = Color.Red;
                        changed = true;
                        break;
                    }
                }
            }
            #endregion

            #region Вставить в рейтинг лучший результат по времени
            for (int i = 0; i < 10; i++)
            {

                var s = time[i].Text.Split(' ');
                if (s[1] == "-")
                {
                    time[i].Text = (i + 1).ToString() + ". " + tm;
                    time[i].ForeColor = Color.Red;
                    changed = true;
                    break;
                }
                else
                {
                    int c = Convert.ToInt32(s[1]);
                    if (tm < c)
                    {
                        MoveTime(i);
                        time[i].Text = (i + 1).ToString() + ". " + tm;
                        time[i].ForeColor = Color.Red;
                        changed = true;
                        break;
                    }
                }
            }
            #endregion
        }
        void MoveSteps(int index)
        {
            for(int i = 9;i<index;i--)
            {
                var s2 = steps[i-1].Text.Split(' ');
                steps[i].Text = (i + 1).ToString() + ". " + s2[1];
            }
        }
        void MoveTime(int index)
        {
            for (int i = 9; i < index; i--)
            {
                var s2 = time[i - 1].Text.Split(' ');
                time[i].Text = (i + 1).ToString() + ". " + s2[1];
            }
        }

        private void Rating_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(changed)
            {
                StreamWriter sw = new StreamWriter(Application.StartupPath + "\\rating.rat", false);
                for (int i = 0; i < 10; i++)
                {
                    var s = steps[i].Text.Split(' ');
                    sw.Write(s[1] + " ");
                }
                for (int i = 0; i < 10; i++)
                {
                    var s = time[i].Text.Split(' ');
                    sw.Write(s[1] + " ");
                }
                sw.Close();
            }
        }
        void CheckFile()
        {
            if(!File.Exists(Application.StartupPath + "\\rating.rat"))
            {
                StreamWriter sw = File.CreateText(Application.StartupPath + "\\rating.rat");
                for(int i =0; i<20;i++)
                {
                    sw.Write("- ");
                }
                sw.Close();
            }
        }

    }
}
